import re
import json
import logging
import argparse

import requests
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO,
                    format="[%(levelname)s] %(asctime)s %(process)d %(name)s@%(lineno)d : %(message)s")


def parse_arguments():
    parser = argparse.ArgumentParser(description="Enter a valid json url")
    parser.add_argument("-u", "--url", type=str, help="json url")

    args = parser.parse_args()
    return args


def read_json(url):
    try:
        response = requests.get(url).text
        return json.loads(response)
    except Exception as e:
        logger.error(e)
        return None


def parse_data(input_dict):
    return_dict = dict(people=[], is_success=False)
    if input_dict:
        # the main author
        author = input_dict["author"]["name"] if input_dict.get("author") \
            and input_dict["author"].get("name") else None
        return_dict["people"].append(author)

        # the other contributors
        if input_dict.get("items"):
            for item in input_dict["items"]:
                title = item["title"] if item.get("title") else None
                contributor = re.sub(r"^.*-\s", "", title)
                return_dict["people"].append(contributor)

        if author or len(return_dict["people"]) > 0:
            return_dict["is_success"] = True

    return return_dict


def write_json(data):
    output_json = json.dumps(data, indent=2, sort_keys=True)
    return output_json


def main():
    arguments = parse_arguments()
    url = arguments.url
    json_content = read_json(url)
    data = parse_data(json_content)
    output_json = write_json(data)
    print(output_json)


if __name__ == "__main__":
    main()