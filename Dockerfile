FROM python:3

RUN mkdir /app

ADD . /app

WORKDIR /app

RUN pip install --no-cache-dir -r requirements.txt

CMD ["python", "/app/json_reader.py", "-u http://therecord.co/feed.json"]